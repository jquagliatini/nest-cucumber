import { IWorld as CucumberWorld } from '@cucumber/cucumber';
import { APIRequestContext } from '@playwright/test';

export interface IWorld extends CucumberWorld {
  request: APIRequestContext;
}
