/* eslint-disable @typescript-eslint/no-var-requires */
const { Before, BeforeAll, AfterAll } = require('@cucumber/cucumber');
const { request: req } = require('playwright');
const { spawn } = require('node:child_process');
const path = require('node:path');
const { default: compileSwcDir } = require('@swc/cli/lib/swc/dir');
const { DEFAULT_EXTENSIONS } = require('@swc/core');

let process;
BeforeAll(async () => {
  const request = await req.newContext({
    baseURL: 'http://localhost:3000',
  });
  let response = await failsafe(() => request.get('/_health'));
  if (response?.ok()) {
    return;
  }

  console.time('compilation');
  for (const folder of ['src', 'tests']) {
    await compileTs(folder);
  }
  console.timeEnd('compilation');

  process = spawn('node', [path.join('dist-swc', 'tests', 'server', 'index')]);
  process.unref();
  console.log(process.pid);

  const wait = (ms) => new Promise((r) => setTimeout(r, ms));
  do {
    await wait(1_000);
    response = await failsafe(async () => {
      const res = await request.get('/_health');
      console.log({ res: res.ok() });
      return res;
    });
  } while (!response?.ok());
});

Before(
  /** @type {import('../world').IWorld} */
  async function () {
    this.request = await req.newContext({
      baseURL: 'http://localhost:3000',
    });
  },
);

AfterAll(async () => {
  if (process) {
    process.kill('SIGTERM');
  }
});

async function failsafe(call) {
  return call().catch(() => {
    /*do nothing*/
  });
}

async function compileTs(folder) {
  await compileSwcDir({
    cliOptions: {
      filenames: [path.join(__dirname, '..', '..', folder)],
      outDir: path.join(__dirname, '..', '..', 'dist-swc', folder),
      copyFiles: true,
      outFile: undefined,
      filename: undefined,
      sync: false,
      sourceMapTarget: undefined,
      extensions: DEFAULT_EXTENSIONS,
      watch: false,
      includeDotfiles: false,
      deleteDirOnStart: false,
      quiet: false,
    },
    swcOptions: {
      jsc: {
        parser: undefined,
        transform: {},
      },
      sourceFileName: undefined,
      sourceRoot: undefined,
      configFile: path.join(__dirname, '..', '..', '.swcrc'),
      swcrc: undefined,
    },
  });
}
