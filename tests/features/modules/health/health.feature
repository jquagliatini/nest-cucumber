Feature: Health

  Scenario: Checking API health
    When I make a 'GET' request
      | url          | /_health                                     |
      | responseFile | modules/health/fixtures/health-ok.fixture.json |
