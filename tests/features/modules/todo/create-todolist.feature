Feature: Creating todolists

  Scenario: Creating a simple todolist
    When I make a 'POST' request
      | url      | /todolists                                                 |
      | response | { "status": "ok", "data": { "todolist": { "id": "<>" } } } |
