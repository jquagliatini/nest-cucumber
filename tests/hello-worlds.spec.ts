import { test, expect } from '@playwright/test';

test('should greet', async ({ request }) => {
  const body = await request.get('/hello');
  await expect(body.json()).resolves.toEqual({ hello: 'world' });
});
