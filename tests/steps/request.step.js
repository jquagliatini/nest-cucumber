// @ts-check
/* eslint-disable @typescript-eslint/no-var-requires */
const { When } = require('@cucumber/cucumber');
const { expect } = require('@playwright/test');
const fs = require('node:fs/promises');
const path = require('node:path');

/** @typedef {'get'|'post'|'put'|'patch'|'delete'} HttpVerb */

/**
 * @param {string} verb
 * @return {verb is HttpVerb}
 */
function isHttpVerb(verb) {
  return ['get', 'post', 'put', 'patch', 'delete'].includes(verb.toLowerCase());
}

function jsonWithPlaceholder(json) {
  return Object.fromEntries(
    Object.entries(json).map(([key, value]) => {
      return [
        key,
        Array.isArray(value)
          ? value.map(jsonWithPlaceholder)
          : typeof value === 'object' && value != null
          ? jsonWithPlaceholder(value)
          : typeof value === 'string' && value === '<>'
          ? expect.any(String)
          : value,
      ];
    }),
  );
}

async function loadJsonFile(jsonFile) {
  return fs
    .readFile(
      path.join(__dirname, '..', 'features', jsonFile.replace(/\//g, path.sep)),
      'utf-8',
    )
    .then(JSON.parse)
    .then(jsonWithPlaceholder);
}

When(
  'I make a {string} request',
  /**
   * @this {import('../../tests/world').IWorld}
   * @param {String} verb
   * @param {import('@cucumber/cucumber').DataTable} dataTable
   */
  async function (verb, dataTable) {
    if (!isHttpVerb(verb)) {
      throw new Error(`${verb} is not a valid HTTP verb`);
    }

    const { url, body, bodyFile, response, responseFile } =
      dataTable.rowsHash();
    if (!url) {
      throw new Error(
        `expected an URL in url field of datable: ${dataTable.rowsHash()}`,
      );
    }

    const requestResponse = await this.request[verb.toLowerCase()](
      url,
      body || bodyFile
        ? { data: body ?? (await loadJsonFile(bodyFile)) }
        : undefined,
    );

    expect(requestResponse.ok()).toBe(true);

    if (response) {
      await expect(requestResponse.json()).resolves.toEqual(
        jsonWithPlaceholder(JSON.parse(response)),
      );
    } else if (responseFile) {
      const responseFileContent = await loadJsonFile(responseFile);
      await expect(requestResponse.json()).resolves.toEqual(
        responseFileContent,
      );
    }
  },
);
