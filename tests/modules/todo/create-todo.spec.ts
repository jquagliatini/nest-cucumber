import { test, expect } from '@playwright/test';

test('should create a todolist', async ({ request }) => {
  const response = await request.post('/todolists');
  expect(response.ok()).toBe(true);
  await expect(response.json()).resolves.toEqual({
    status: 'ok',
    data: { todolist: { id: expect.any(String) } },
  });
});
