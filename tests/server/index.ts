import { Logger } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { AppModule } from '../../src/modules/core/app';

async function startTestServer() {
  const container = await Test.createTestingModule({
    imports: [AppModule],
  }).compile();
  const app = await container.createNestApplication().init();

  app.useLogger(['log', 'verbose', 'error', 'warn']);

  await app.listen(3000);
}

const logger = new Logger('Integration tests');
startTestServer().catch((e) => {
  logger.error(e);
});
