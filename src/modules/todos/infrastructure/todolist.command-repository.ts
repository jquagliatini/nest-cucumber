import { Repository } from '../../core/shared/types';
import { TodolistAggregate } from '../domain';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface TodolistCommandRepository
  extends Repository<TodolistAggregate> {}
