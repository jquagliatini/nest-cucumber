import { Injectable } from '@nestjs/common';
import { PrismaService, PrismaCommandRepository } from '../../core/shared';
import { TodolistAggregate } from '../domain';
import { ListCreatedEvent } from '../domain/events';
import { TodolistCommandRepository } from './todolist.command-repository';

@Injectable()
export class TodolistPrismaCommandRepository
  extends PrismaCommandRepository<TodolistAggregate>
  implements TodolistCommandRepository
{
  constructor(prisma: PrismaService) {
    super(prisma);

    this.on(ListCreatedEvent, this.persistListCreatedEvent);
  }

  private persistListCreatedEvent({ id }: ListCreatedEvent) {
    return this.prisma.list.create({
      data: { id },
    });
  }
}
