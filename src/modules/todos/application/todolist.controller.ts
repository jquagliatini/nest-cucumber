import { Controller, Post } from '@nestjs/common';
import { CommandBus } from '@nestjs/cqrs';
import { CreateListCommand } from '../domain/commands/create-list/create-list.command';

@Controller('todolists')
export class TodolistController {
  constructor(private readonly commandBus: CommandBus) {}

  @Post()
  createList() {
    return this.commandBus.execute(new CreateListCommand()).then((data) => ({
      status: 'ok',
      data: { todolist: data },
    }));
  }
}
