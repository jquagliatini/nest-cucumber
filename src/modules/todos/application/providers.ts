import { Provider } from '@nestjs/common';
import { TODOLIST_COMMAND_REPOSITORY } from './tokens';
import { TodolistPrismaCommandRepository } from '../infrastructure/todolist.prisma-command-repository';
import { CreateListCommandHandler } from '../domain/commands/create-list/create-list.handler';

export const TodolistProviders: Provider[] = [
  CreateListCommandHandler,
  {
    provide: TODOLIST_COMMAND_REPOSITORY,
    useClass: TodolistPrismaCommandRepository,
  },
];
