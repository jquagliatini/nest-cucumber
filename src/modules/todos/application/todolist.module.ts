import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TodolistProviders } from './providers';
import { TodolistController } from './todolist.controller';

@Module({
  imports: [CqrsModule],
  controllers: [TodolistController],
  providers: TodolistProviders,
})
export class TodolistModule {}
