import { DomainEvent } from '../../../core/shared/types';

export class ListCreatedEvent extends DomainEvent {
  constructor(public readonly id: string) {
    super();
  }
}
