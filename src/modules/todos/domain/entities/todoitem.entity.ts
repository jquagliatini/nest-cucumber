export class TodoitemEntity {
  constructor(
    public readonly id: string,
    public readonly label: string,
    public readonly checked: boolean = false,
  ) {}

  check() {
    return new TodoitemEntity(this.id, this.label, true);
  }

  uncheck() {
    return new TodoitemEntity(this.id, this.label, false);
  }

  relabel(newLabel: string) {
    return new TodoitemEntity(this.id, newLabel, this.checked);
  }
}
