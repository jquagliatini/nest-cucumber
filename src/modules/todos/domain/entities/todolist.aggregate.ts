import { AggregateRoot } from '@nestjs/cqrs';
import { randomUUID } from 'crypto';
import { ListCreatedEvent } from '../events';
import { TodoitemEntity } from './todoitem.entity';

export class TodolistAggregate extends AggregateRoot<ListCreatedEvent> {
  private constructor(
    public readonly id: string,
    public readonly items: readonly TodoitemEntity[],
  ) {
    super();
  }

  static from({ id, items = [] }: TodolistProps) {
    return new TodolistAggregate(
      id,
      items.map(
        ({ id, label = '', checked = false }) =>
          new TodoitemEntity(id, label ?? '', checked ?? false),
      ),
    );
  }

  static create() {
    const self = TodolistAggregate.from({ id: randomUUID() });
    self.apply(new ListCreatedEvent(self.id));
    return self;
  }
}

export type TodolistProps = {
  id: string;
  items?: readonly TodoitemProps[];
};

type TodoitemProps = {
  id: string;
  label?: string | null;
  checked?: boolean | null;
};
