import { Command } from '../../../../core/shared/types';

export class CreateListCommand extends Command<CreateListResponse> {}

export type CreateListResponse = { id: string };
