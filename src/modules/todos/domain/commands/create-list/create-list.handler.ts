import { Inject } from '@nestjs/common';
import { CommandHandler } from '@nestjs/cqrs';
import { ICommandHandler } from '../../../../core/shared/types';
import {
  TODOLIST_COMMAND_REPOSITORY,
  TodolistCommandRepository,
} from '../../../../todos';
import { TodolistAggregate } from '../../entities';
import { CreateListCommand } from './create-list.command';

@CommandHandler(CreateListCommand)
export class CreateListCommandHandler
  implements ICommandHandler<CreateListCommand>
{
  constructor(
    @Inject(TODOLIST_COMMAND_REPOSITORY)
    private readonly todolistCommandRepository: TodolistCommandRepository,
  ) {}

  async execute() {
    const todo = TodolistAggregate.create();
    await this.todolistCommandRepository.persist(todo);

    return { id: todo.id };
  }
}
