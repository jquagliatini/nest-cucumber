import { Controller, Get } from '@nestjs/common';

@Controller()
export class GreetingsController {
  @Get('hello')
  hello() {
    return { hello: 'world' };
  }
}
