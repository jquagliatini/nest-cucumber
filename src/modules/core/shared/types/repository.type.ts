import { AggregateRoot } from '@nestjs/cqrs';

export interface Repository<T extends AggregateRoot> {
  persist(aggregate: T): Promise<unknown>;
}
