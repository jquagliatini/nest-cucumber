import { ICommand } from '@nestjs/cqrs';

export abstract class Command<Output = unknown> implements ICommand {
  __return_type: Output;
}

export type CommandReturnType<C extends Command> = C['__return_type'];
