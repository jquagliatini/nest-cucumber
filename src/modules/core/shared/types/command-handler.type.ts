import { ICommandHandler as NestICommandHandler } from '@nestjs/cqrs';
import { Command, CommandReturnType } from './command.type';

export type ICommandHandler<C extends Command> = NestICommandHandler<
  C,
  CommandReturnType<C>
>;
