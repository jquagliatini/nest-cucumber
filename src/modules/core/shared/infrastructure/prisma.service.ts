import { PrismaClient } from '@prisma/client';
import {
  Global,
  INestApplication,
  Injectable,
  Logger,
  OnModuleInit,
} from '@nestjs/common';

@Global()
@Injectable()
export class PrismaService extends PrismaClient implements OnModuleInit {
  private logger = new Logger(PrismaService.name);

  async onModuleInit() {
    this.logger.warn(`$connecting`);
    await this.$connect();
  }

  enableShutdownHooks(app: INestApplication) {
    this.$on('beforeExit', async () => {
      await app.close();
    });
  }
}
