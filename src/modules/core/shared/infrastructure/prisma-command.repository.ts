import { Logger } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { PrismaPromise } from '@prisma/client';
import { DomainEvent, Repository } from '../types';
import { PrismaService } from './prisma.service';

export abstract class PrismaCommandRepository<
  T extends AggregateRoot<DomainEvent> = AggregateRoot,
  AggregateEvent = ReturnType<T['getUncommittedEvents']>[number],
> implements Repository<T>
{
  private logger = new Logger(PrismaCommandRepository.name);

  private persistors: Map<
    DomainEventCtor<AggregateEvent>,
    Persistor<AggregateEvent, unknown>[]
  > = new Map();

  protected constructor(protected prisma: PrismaService) {}

  persist(aggregate: T) {
    return this.prisma.$transaction(
      aggregate.getUncommittedEvents().flatMap((event) => {
        const persistors = this.persistors.get(
          event.constructor as DomainEventCtor<AggregateEvent>,
        );
        if (!persistors) {
          this.logger.warn(
            `No persistor defined for event [${event.constructor.name}]`,
          );
          return [];
        }

        return persistors.map((persistor) =>
          persistor(event as AggregateEvent),
        );
      }),
    );
  }

  on<E extends DomainEventCtor<AggregateEvent>, Output = void>(
    event: E,
    persistor: Persistor<AggregateEvent, Output>,
  ): void {
    this.persistors.set(
      event,
      (this.persistors.get(event) ?? []).concat(persistor.bind(this)),
    );
  }
}

type DomainEventCtor<E extends DomainEvent = DomainEvent> = {
  new (...args: unknown[]): E;
};
type Persistor<E extends DomainEvent, Output = void> = (
  event: E,
) => PrismaPromise<Output>;
