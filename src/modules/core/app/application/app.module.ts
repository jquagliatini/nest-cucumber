import { Module } from '@nestjs/common';

import { PrismaModule } from './prisma.module';
import { HealthModule } from '../../health';

import { GreetingsModule } from '../../../greetings';
import { TodolistModule } from '../../../todos';

@Module({
  imports: [PrismaModule, GreetingsModule, HealthModule, TodolistModule],
})
export class AppModule {}
