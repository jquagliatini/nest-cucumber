import { Controller, Get } from '@nestjs/common';

@Controller('_health')
export class HealthController {
  @Get()
  ping() {
    return { status: 'ok' };
  }
}
