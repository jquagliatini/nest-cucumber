// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path');

module.exports = {
  default: {
    paths: [path.join(__dirname, 'tests', 'features', '**', '*.feature')],
    require: [
      path.join(__dirname, 'tests', 'features', 'hooks.js'),
      path.join(__dirname, 'tests', 'steps', '**', '*.js'),
    ],
    publishQuiet: true,
  },
};
