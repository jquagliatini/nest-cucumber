-- CreateTable
CREATE TABLE "todos" (
    "id" UUID NOT NULL,
    "label" VARCHAR NOT NULL,
    "order" SMALLINT NOT NULL,
    "done" BOOLEAN NOT NULL DEFAULT false,
    "list_id" UUID NOT NULL,

    CONSTRAINT "todos_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "lists" (
    "id" UUID NOT NULL,

    CONSTRAINT "lists_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "todos" ADD CONSTRAINT "todos_list_id_fkey" FOREIGN KEY ("list_id") REFERENCES "lists"("id") ON DELETE CASCADE ON UPDATE NO ACTION;
